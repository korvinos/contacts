from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Contact(models.Model):
    first_name = models.CharField(null=True, blank=True, max_length=100, verbose_name=u'Имя')
    last_name = models.CharField(null=True, blank=True, max_length=100, verbose_name=u'Фамилия')
    middle_name = models.CharField(null=True, blank=True, max_length=100, verbose_name=u'Отчество')
    email = models.EmailField(null=True, blank=True, max_length=100,  verbose_name=u'E-mail')
    telephone = PhoneNumberField(blank=True, verbose_name=u'Номер телефона')
    birthday = models.DateField(blank=True, null=True, verbose_name=u'Дата рождения')

    def delete(self, *args, **kwargs):
        super(Contact, self).delete(*args, **kwargs)

    class Meta:
        default_related_name = 'Contacts'
