from django.shortcuts import render, get_object_or_404
from .models import Contact
from .forms import ContactForm
from django.http.response import HttpResponseRedirect


def index(request):
    contacts = Contact.objects.all()

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = form.save()
            contact.save()
            return HttpResponseRedirect('/')

    else:
        form = ContactForm()

    context = {'contacts': contacts,
               'form': form}
    return render(request, 'project/index.html', context)


def delete(request, contact_pk):
    contact = Contact.objects.get(pk=contact_pk)
    contact.delete(save=False)
    return HttpResponseRedirect('/')


def update(request, contact_pk):
    contact = Contact.objects.get(pk=contact_pk)
    if request.method == "POST":
        form = ContactForm(request.POST, instance=contact)
        if form.is_valid():
            contact.save()

            return HttpResponseRedirect('/')

    else:
        form = ContactForm(instance=contact)

    contacts = Contact.objects.all()
    context = {'contacts': contacts,
               'form': form}

    return render(request, 'project/index.html', context)