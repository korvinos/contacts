from django.conf.urls import url
from .views import index, delete, update
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^$', index),
    url(r'^del/(?P<contact_pk>[0-9]+)/$', delete),
    url(r'^update/(?P<contact_pk>[0-9]+)/$', update)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)