from django.forms import ModelForm
from phonenumber_field.formfields import PhoneNumberField
from .models import Contact


class ContactForm(ModelForm):
    class Meta:
        model = Contact
        fields = ['last_name', 'first_name', 'middle_name', 'email', 'telephone', 'birthday']

# class ContactForm(forms.Form):
#     first_name = forms.CharField(max_length=100, label=u'Имя')
#     last_name = forms.CharField(max_length=100, label=u'Фамилия')
#     middle_name = forms.CharField(max_length=100, label=u'Отчество')
#     email = forms.EmailField(max_length=100, label=u'E-mail')
#     telephone = PhoneNumberField(label=u'Телефон')
#     birthday = forms.DateField(label=u'День рождения')
#
#     def save(self):
#         contact = Contact(**self.cleaned_data)
#         contact.save()
#         return contact
