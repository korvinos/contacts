# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-14 17:18
from __future__ import unicode_literals

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Имя')),
                ('last_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Фамилия')),
                ('middle_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Отчество')),
                ('email', models.EmailField(blank=True, max_length=100, null=True, verbose_name='E-mail')),
                ('telephone', phonenumber_field.modelfields.PhoneNumberField(blank=True, max_length=128, verbose_name='Номер телефона')),
                ('birthday', models.DateField(blank=True, verbose_name='Дата рождения')),
            ],
            options={
                'default_related_name': 'Contacts',
            },
        ),
    ]
